# imx-tlv320aic23
ALSA SoC machine driver for binding TLV320AIC23 codec driver on IMX platfomrs.

===

This driver just binds the Texas Instruments TLV320AIC23 driver on iMX platforms. I build for the [Udoo iMX6Q](http://udoo.org/), but it should work on pretty much any iMX6 platform.

Just add it to the device tree:

    sound {
        compatible = "fsl,imx-audio-tlv320aic23";
        model = "tlv320aic23";
        ssi-controller = <&ssi1>;
        audio-codec = <&tlv320>;
        audio-routing =
            "Headphone Jack", "LHPOUT",
            "Headphone Jack", "RHPOUT",
            "Line Out Jack", "LOUT",
            "Line Out Jack", "ROUT";
        mux-int-port = <2>;
        mux-ext-port = <3>;
    };

Currently only tested on 3.10 Freescale BSP kernels, but I'll be updating this soon (if needed) for the 3.14 BSP. 

I'm going to add my linux repo soon with this driver integrated, but the 0001-Makefile-and-Kconfig-addons-for-imx-tlv320aic23-mach patch can be used to add this driver into the kernel build files directly. The patch was created from the imx_3.10.53_1.1.0_ga Freescale branch and just sets up the sound/soc/fsl/Kconfig and sound/soc/fsl/Makefile. You would still need to copy the driver to sound/soc/fsl to build this in the kernel.
