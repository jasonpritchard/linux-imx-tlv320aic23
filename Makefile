obj-m := imx-tlv320aic23.o

KERNELDIR ?= /space/embedded/elinux/kernel/linux-imx
PWD       := $(shell pwd)

CFLAGS_imx-tlv320aic23.o := -DDEBUG
EXTRA_CFLAGS	         := -DDEBUG=1

all:
	$(MAKE) -C $(KERNELDIR) M=$(PWD)

modules_install:
	$(MAKE) -C $(KERNELDIR) M=$(PWD) modules_install

clean:
	rm -rf *.o *~ core .depend .*.cmd *.ko *.mod.c .tmp_versions vtty modules.order Module.symvers

